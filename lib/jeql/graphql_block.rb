class Jeql::GraphqlBlock < Liquid::Block
  GraphQlError = Class.new(Jekyll::Errors::FatalException)
  PARAMS_SYNTAX = /(\w+):\s*['"](\w+)['"],?/

  def initialize(tag_name, text, tokens)
    super
    @params = text.scan(PARAMS_SYNTAX)
    @text = text
  end

  def render(context)
    jekyll_site = context.registers[:site]

    query_param = if context.key?(hash_params["query"])
                    context.find_variable(hash_params["query"])
                  else
                    hash_params["query"]
                  end

    endpoint_param = if context.key?(hash_params["endpoint"])
                      context.find_variable(hash_params["endpoint"])
                     else
                      hash_params["endpoint"]
                     end

    gql_query = Jeql::Query.new(query_param,
                            jekyll_site.config["source"],
                            jekyll_site.config["jeql"][endpoint_param])

    if gql_query.response.success?
      context['data'] = JSON.parse(gql_query.response.body)['data']
      super
    else
      raise GraphQlError, "The query #{gql_query.query_name} failed"
    end
  end

  private

  def hash_params
    @hash_params ||= begin
      if @params.empty?
        Hash[@text.split(',').map { |row| row.split(':').map{ |col| col.delete(' ') }}]
      else
        Hash[@params]
      end
    end
  end
end

Liquid::Template.register_tag('graphql', Jeql::GraphqlBlock)
